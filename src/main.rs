#![allow(unused, dead_code)]
use std::fs::read_to_string;

use mlua::prelude::*;

#[derive(Clone)]
struct LuaVectorF64 {
    data: Vec<f64>
}

impl LuaVectorF64 {
    fn new() -> Self {
        Self { data: Vec::new() }
    }
}

impl LuaUserData for LuaVectorF64 {
    fn add_methods<'lua, M: LuaUserDataMethods<'lua, Self>>(methods: &mut M) {
        methods.add_method.mut (mlua::MetaMethod::ToString, |lua_ctx, (object):(LuaVectorF64)| {
            Ok(format!("{:?}", object.data))
        }
        methods.add_meta_function(mlua::MetaMethod::ToString, |lua_ctx, (object):(LuaVectorF64)| {
            Ok(format!("{:?}", object.data))
        }

    }
}

fn main() {
    let lua = mlua::Lua::new();
    let globals = lua.globals();
    let source_code = read_to_string("main.lua").unwrap();

    let table = lua.create_table().unwrap();
    let vecf64 = lua.create_function(|lua_ctx, ()| {
        Ok(LuaVectorF64::new())
    }).unwrap();

    table.set("VecF64", vecf64).unwrap();
    globals.set("Table", table).unwrap();

  
    lua.load(&source_code).exec().unwrap();
}
